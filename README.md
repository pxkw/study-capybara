# Overview

Introduction to run e2e tests using Capybara, RSpec and Phantomjs.

# Reqirements

  + Phantomjs
  + Ruby, bundler

# QuickStart

```
cd test
bundle install --path vendor/bundle
bundle exec rspec spec/index_spec.rb 
```

# Installing Ruby and bundler

**for OSX**

```
brew install rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo `eval "$(rbenv init -)"` >> ~/.bashrc
source ~/.bashrc
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build 
brew tap homebrew/versions ; brew install llvm35
rbenv install -l  # ==> Choose a version to install
rbenv install rbx-2.5.8
rbenv global rbx-2.5.8
ruby -v  # ==> Check the version to be used
gem install bundler
```

# References

  + Installation and configuration
    - (JA) http://back.hatenablog.com/entry/2015/09/13/160242
  + Coding tests
    - (JA) http://blog.enogineer.com/2014/10/11/rails-capybara-have-content/
    - (EN) https://gist.github.com/them0nk/2166525

