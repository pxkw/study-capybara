# encoding: utf-8
require 'spec_helper'

describe 'google.com' do
  include Capybara::DSL

  before { visit '/' }

  scenario 'should be accessible' do
    page.driver.render('./screenshot.png', :full => true)
    puts page.title
    current_url.should match /google/
  end

  subject { page }
  it { have_title 'Google' }
  it { have_css 'body', text: 'Google' }
end
