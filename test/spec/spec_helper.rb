# encoding: utf-8
require 'capybara/rspec'
require 'capybara/poltergeist'

Capybara.default_driver = :poltergeist
Capybara.app_host = 'http://google.com'
Capybara.run_server = false
Capybara.javascript_driver = :poltergeist

# Capybara.register_driver :poltergeist do |app|
#   Capybara::Poltergeist::Driver.new(app, :js_errors => false, :timeout => 60)
# end

